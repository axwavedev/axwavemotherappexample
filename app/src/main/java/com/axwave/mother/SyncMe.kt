package com.axwave.mother

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast

class SyncMe : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sync_me)

        Toast.makeText(this, "syncing", Toast.LENGTH_LONG).show()

        val returnIntent: Intent = intent;
        returnIntent.putExtra("KEY_USERID", "TEST_TEST")
        returnIntent.putExtra("KEY_USERID", "LUTH")
        returnIntent.putExtra("KEY_ACR_CUSTOM_USER_IS_ELIGIBLE", true)
        setResult(RESULT_OK, returnIntent)
        finish()
    }

}
