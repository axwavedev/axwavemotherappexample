package com.axwave.mother

class SharedObject(val exceptionMessage: String = "",
                   val pluginIsWorking: Boolean = false,
                   val howManyFpDidSendDuringLastHour: Long = 0)
