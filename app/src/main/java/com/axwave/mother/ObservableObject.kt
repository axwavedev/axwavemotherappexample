package com.axwave.mother

import java.util.Observable

class ObservableObject : Observable() {

    fun updateValue(data: Any) {
        synchronized(this) {
            setChanged()
            notifyObservers(data)
        }
    }

    companion object {

        private val instance = ObservableObject()

        fun get(): ObservableObject {
            return instance
        }
    }

}
