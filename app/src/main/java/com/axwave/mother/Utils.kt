package com.axwave.mother

import android.annotation.SuppressLint
import android.app.Activity
import android.content.*
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.support.v7.app.AlertDialog
import android.util.Log
import android.widget.Toast
import com.google.gson.Gson
import java.text.SimpleDateFormat

class Utils {

    companion object {

        val TAG = "MotherApp"

        fun startPlugin(context: Context): Boolean {
            Log.d(TAG, "startPlugin called")
            val intent = Intent(Intent.ACTION_MAIN)
            intent.component = ComponentName(MainActivity.PLUGIN_PACKAGE_NAME, "${MainActivity.PLUGIN_PACKAGE_NAME}.MainActivity")
            intent.putExtra("KEY_REWARD_NAME", "Axwave Plugin")
            intent.putExtra("KEY_REWARD_VALUE", "20")
            intent.putExtra("CONFIG_USERNAME", "test@eliasz.com")

            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK

            try {
                context.startActivity(intent)
                return true;
            } catch (exception: Exception) {
                Toast.makeText(context, "You don't have this app", Toast.LENGTH_LONG).show()
                return false;
            }
        }

        fun installationFilter(): IntentFilter {
            val intentFilter = IntentFilter()
            intentFilter.addAction(Intent.ACTION_PACKAGE_ADDED)
            intentFilter.addAction(Intent.ACTION_PACKAGE_INSTALL)
            intentFilter.addDataScheme("package")
            return intentFilter
        }

        fun sendSimpleBroadcast(context: Context, method: String) {
            val intent = Intent()
            intent.action = "com.axwave.communication"
            intent.putExtra(method, method)
            context.sendBroadcast(intent)
        }

        fun youHaveToDownloadPlugin(activity: Activity) {
            val packageName: String = "com.axwave.plugin";
            getAlertDialog(activity).setTitle("Install Plugin")
                    .setCancelable(false)
                    .setMessage("You have to download and install Axwave Plugin Extension to enable this feature and collect points. ")
                    .setPositiveButton("Download") { _, _ ->
                        Toast.makeText(activity, "If Plugin will be released you will be able to download it from store", Toast.LENGTH_LONG).show()
                        Toast.makeText(activity, "Plugin will open automatically after installation by registered BroadcastReceiver", Toast.LENGTH_LONG).show()

                        try {
                            activity.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$packageName")))
                        } catch (ignore: android.content.ActivityNotFoundException) {
                            activity.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=$packageName")))
                        }

                        activity.finish()
                    }
                    .setNegativeButton("Not now") { dialog: DialogInterface, _ ->
                        dialog.dismiss()
                    }.show()
        }


        fun getAlertDialog(context: Context): AlertDialog.Builder {
            val builder: AlertDialog.Builder = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                AlertDialog.Builder(context, android.R.style.Theme_Material_Dialog_Alert)
            } else {
                AlertDialog.Builder(context)
            }
            return builder
        }

        fun isPackageExisted(context: Context, targetPackage: String): Boolean {
            val mainIntent = Intent(Intent.ACTION_MAIN, null)
            mainIntent.addCategory(Intent.CATEGORY_LAUNCHER)
            val pm = context.packageManager;
            val pkgAppsList = pm.queryIntentActivities(mainIntent, 0)
            for (resolveInfo in pkgAppsList) {

                var packageInfo: PackageInfo? = null
                try {
                    packageInfo = pm.getPackageInfo(resolveInfo.activityInfo.packageName,
                            PackageManager.GET_PERMISSIONS)
                } catch (ignore: PackageManager.NameNotFoundException) {
                    //
                }

                if (packageInfo == null) continue
                if (packageInfo.packageName == targetPackage) return true
            }
            return false
        }

    }

    class PluginCommunicationReceiver : BroadcastReceiver() {

        @SuppressLint("SimpleDateFormat")
        override fun onReceive(context: Context?, intent: Intent?) {
            val time = SimpleDateFormat("hh:mma").format(System.currentTimeMillis())
            val json = intent?.extras?.getString("SHARED_OBJECT")
            json?.let {
                val sharedObject = Gson().fromJson(it, SharedObject::class.java)
                ObservableObject.get().updateValue(sharedObject)

                Log.d(TAG, "received $time ${sharedObject.howManyFpDidSendDuringLastHour}")
            }
        }
    }

    class DetectPluginInstalled : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val packageName = intent?.getData()?.getEncodedSchemeSpecificPart()
            Log.d(TAG, "installed $packageName")
            MainActivity.mPluginInstalled = true
            if (packageName.equals("com.axwave.acr")) context?.let { Utils.startPlugin(context) }
        }
    }

}
