package com.axwave.mother

import android.content.IntentFilter
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity(), Observer {

    val TAG = "MotherApp";

    val mHandler = Handler()

    companion object {
        val PLUGIN_PACKAGE_NAME = "com.axwave.plugin"
        var mPluginInstalled = false
        var mPluginWorking = false
    }

    private val pluginCommunicationReceiver = Utils.PluginCommunicationReceiver()

    private val detectPluginInstalled = Utils.DetectPluginInstalled()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mPluginInstalled = Utils.isPackageExisted(this, PLUGIN_PACKAGE_NAME)

        ObservableObject.get().addObserver(this)

        enable_plugin.setOnClickListener {
            if (mPluginWorking) {
                Utils.sendSimpleBroadcast(this, "STOP_SDK")
            } else {
                if (!Utils.startPlugin(this))
                    Utils.youHaveToDownloadPlugin(this)
            }
        }

        if (mPluginInstalled) {
            mHandler.postDelayed({
                if (loading.visibility == View.VISIBLE) loading.visibility = View.GONE
            }, TimeUnit.SECONDS.toMillis(5))
        } else {
            loading.visibility = View.GONE
        }

        applicationContext.registerReceiver(pluginCommunicationReceiver,
                IntentFilter("com.axwave.communication"))
        applicationContext.registerReceiver(detectPluginInstalled,
                Utils.installationFilter())
    }

    override fun onResume() {
        super.onResume()
        mPluginInstalled = Utils.isPackageExisted(this, PLUGIN_PACKAGE_NAME);
    }

    override fun onDestroy() {
        ObservableObject.get().deleteObserver(this)
        applicationContext.unregisterReceiver(pluginCommunicationReceiver)
        applicationContext.unregisterReceiver(detectPluginInstalled)
        mHandler.removeMessages(0)
        super.onDestroy()
    }

    override fun update(p0: Observable?, p1: Any?) {
        val time = SimpleDateFormat("hh:mma").format(System.currentTimeMillis());
        Log.d("MainActivity", "time $time")
        p1?.let {
            val sharedObject: SharedObject = it as SharedObject

            Log.d("MainActivity", "is working ${it.pluginIsWorking} stats ${it.howManyFpDidSendDuringLastHour}")

            runOnUiThread {
                if (loading.visibility == View.VISIBLE) loading.visibility = View.GONE
                if (sharedObject.exceptionMessage.isNotEmpty()) {
                    addLog("$time ${sharedObject.exceptionMessage}")
                }

                addLog("$time : ${sharedObject.howManyFpDidSendDuringLastHour}")
                mPluginWorking = sharedObject.pluginIsWorking
                if (mPluginWorking) {
                    enable_plugin.text = "disable plugin";
                } else {
                    enable_plugin.text = "enable plugin";

                }
            }
        }
    }

    fun addLog(message: String) {
        textView.text = "$message \n${textView.text}"
    }

}
